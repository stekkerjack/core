<?php

namespace PhpIntegrator\Refactoring;

/**
 * Indicates the name of the use statement matches the name of the containing namespace.
 */
class UseStatementEqualsNamespaceException extends UseStatementInsertionCreationException
{

}
