<?php

namespace PhpIntegrator\Utility\Typing;

/**
 * Represents a special (non-class) type.
 *
 * {@inheritDoc}
 */
final class SpecialType extends Type
{

}
